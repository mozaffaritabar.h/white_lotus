import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TransactionsService {
  $addEmitter = new EventEmitter();
  $updateEmitter = new EventEmitter();

  constructor() {}

  addTransaction(transaction) {
    this.$addEmitter.emit(transaction);
  }
}
