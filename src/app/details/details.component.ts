import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TransactionsService } from '../transactions.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  API_ENDPOINT = 'http://127.0.0.1:5002/';
  id: string;
  transaction = {
    to: '',
    amount: '',
    date: '',
    status: '',
  };

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private service: TransactionsService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.http
      .get(`${this.API_ENDPOINT}read?id=${this.id}`)
      .subscribe((data: any[]) => (this.transaction = data[0]));
  }

  updateTransaction(transaction: object) {
    this.http.post(`${this.API_ENDPOINT}update`, transaction).subscribe();
  }

  changeStatus(status: string) {
    this.transaction.status = status;
    this.updateTransaction(this.transaction);
  }
}
