import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransactionsService } from '../transactions.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  directions = {
    date: '',
    beneficiary: '',
    amount: '',
  };
  keyword = '';
  transactions: any[] = [];
  transactionsRef: any[] = [];
  API_ENDPOINT = 'http://127.0.0.1:5002/';
  private addSubscribe: Subscription;
  private updateSubscribe: Subscription;

  constructor(private http: HttpClient, private service: TransactionsService) {}

  ngOnInit(): void {
    this.addSubscribe = this.service.$addEmitter.subscribe((data) => {
      this.addData(data);
    });
    this.fetchData();
  }

  ngOnDestroy() {
    this.addSubscribe.unsubscribe();
  }

  fetchData() {
    this.http.get(`${this.API_ENDPOINT}read`).subscribe((data: any[]) => {
      this.transactions = data;
      this.transactionsRef = data;
    });
  }

  getData(id = null) {
    if (!id) {
      return this.transactions;
    }

    for (const item of this.transactions) {
      if (item._id === id) {
        return item;
      }
    }
  }

  addData(transaction: object) {
    this.http
      .put(`${this.API_ENDPOINT}add`, transaction)
      .subscribe((data: any[]) => {
        this.transactions = data;
        this.keyword = '';
        this.fetchData();
      });
  }

  // Toggle sort icons Up or DOWN
  toggleSort(key: string) {
    for (const i in this.directions) {
      if (i === key) {
        this.directions[i] = this.directions[i] === 'asc' ? 'desc' : 'asc';
      } else {
        this.directions[i] = '';
      }
    }
  }

  // Used for sort functions
  compare(a: any, b: any, direction: string) {
    return direction === 'asc'
      ? a > b
        ? -1
        : a < b
        ? 1
        : 0
      : a > b
      ? 1
      : a < b
      ? -1
      : 0;
  }

  // Sort transactions by DATE
  sortDate() {
    this.transactions.sort((a, b) => {
      return this.compare(
        a.date.replace(/-/g, ''),
        b.date.replace(/-/g, ''),
        this.directions.date
      );
    });
    this.toggleSort('date');
    this.keyword = '';
  }

  // Sort transactions by BENEFICIARY
  sortBeneficiary() {
    this.transactions.sort((a, b) => {
      return this.compare(
        a.to.toLocaleLowerCase(),
        b.to.toLocaleLowerCase(),
        this.directions.beneficiary
      );
    });
    this.toggleSort('beneficiary');
    this.keyword = '';
  }

  // Sort transactions by AMOUNT
  sortAmount() {
    this.transactions.sort((a, b) => {
      return this.compare(
        parseFloat(a.amount),
        parseFloat(b.amount),
        this.directions.amount
      );
    });
    this.toggleSort('amount');
    this.keyword = '';
  }

  search() {
    this.transactions = [...this.transactionsRef];
    this.transactions = this.transactions.filter((transaction) => {
      return transaction.to.toLowerCase().includes(this.keyword.toLowerCase());
    });
  }
}
