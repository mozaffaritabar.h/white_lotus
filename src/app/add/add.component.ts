import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../transactions.service';
import { ListComponent } from '../list/list.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit {
  passes = {
    from: 'pass',
    to: 'pass',
    amount: 'pass',
  };

  transaction = {
    from: '',
    to: '',
    amount: '',
    status: 'sent',
  };

  list: ListComponent;

  constructor(private service: TransactionsService) {}

  ngOnInit(): void {}

  validator() {
    let ret = true;

    for (const i of Object.keys(this.passes)) {
      if (this.transaction[i].length === 0) {
        this.passes[i] = 'failed';
        ret = false;
      } else {
        this.passes[i] = 'pass';
      }
    }

    return ret;
  }

  reset() {
    for (const i of Object.keys(this.passes)) {
      this.transaction[i] = '';
      this.passes[i] = 'pass';
    }
  }

  insert() {
    if (this.validator()) {
      this.service.addTransaction(Object.assign({}, this.transaction));
      this.reset();
    }
  }
}
