# Front:

    1. Angular
    2. SASS

# Back:

    1. python
    2. pymongo
    3. flask

# DB:

    1. mongoDB
    	1. db name: whiteLotus
    	2. collection: transactions

# CRUD end-point :

    1. location: "/api/CRUD.py"
    2. URL: http://127.0.0.1:5002/

# Test clip :

    /src/test.webm
